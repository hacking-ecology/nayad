[TOC]

## 1. Introduction

Nayad v1 is an agnostic IoT device, bringing hardware security by design to a large range of use cases. The complete set of communication protocols (Wi-Fi, LoRa, Bluetooth) and integrated GPS provides users with a device ready to deploy different projects. 

The integration of a cryptographic hardware is central to providing integrity, authenticity and confidentiality to IoT network. Integrity assure that a message/data was not manipulated in any way, authenticity assure that the device (IoT node and/or gateway) is what it is declared to be, and confidentiality assure that only parts involved in the communication will have access to its content, protecting data. In IoT those are critical features to be provided to IoT devices, but in general only industrial grade devices provide secure features.

## 2. Methodology
The development of Nayad v1 started with a bibliographic research of studies using cryptographic hardware to improve IoT device authentication and other security features.
From the papers (see references), we have analyzed the following criteria: energy and time consumption to process cryptographic operations, reliability of security features, external memory to store information, price, and simplicity to be deployed by end-users.

The final list of security hardware selected three modules to start the prototyping.


|Manufacturer| Model|
|---|---|
|Microchip|ATECC608B-TFLXTLS|
|Microchip|ATECC608B-TNGTLS|
|NXP|SE050|

After the evaluation process, we selected the ATECC608B-TFLXTLS, according to:
- Reliable cryptographic features
- Efficiency
- Compatibility with diverse IoT cloud services
- Price

### 2.1. ATECC608B-TFLXTLSS

The ATECC608B-TFLXTLS provides advanced configuration, compared to the Trust&Go version. It has pre-configured support for Amazon, Google and Microsoft clouds. Additionally, it allows us to upload of custom certificates to support others IoT networks. The project has selected the ATECC608B-TFLXTLS due to its multiple possibility of deployment, according to the use case. The following features are from the [ATECC608B-TFLXTLS documentation](https://onlinedocs.microchip.com/pr/GUID-8F01540E-14A9-479E-9D27-FFFB14DE474E-en-US-1/index.html?GUID-125F1A93-76CC-4BD7-BACA-01844FBD5F4F):

#### 2.1.1 Secure TLS Connection
The ATECC608B-TFLXTLS allows the creation of secure TLS connections using a variety of protocols. The device is capable of establishing secure connections to the Google Cloud, to AWS and other cloud providers. Through the various modes of the Key Derivation Function (KDF), appropriate keys can be generated to support TLS1.2, TLS1.3 and earlier secure connection internet protocols.

#### 2.1.2 Secure Boot
It verifies that the code is authentic and has not been modified, the overall integrity of the system is maintained. The ATECC608B-TFLXTLS has been configured to allow Secure Boot by storing the code digest of the system within a data slot of the device. Upon initial execution of the code, the system can regenerate the digest over the system firmware and compare it with the digest stored in the ATECC608B-TFLXTLS, verifying that the firmware has not been tampered with.

#### 2.1.3 Disposable/Accessory Authentication
It prevents low-cost clones of products that can damage an OEM’s reputation for quality, image in the marketplace and overall profit margins. The ATECC608B-TFLXTLS provides the ability to authenticate these types of products by providing a chain-of-trust from device to Root Certificate Authority.

#### 2.1.4 Data Protection
The ATECC608B-TFLXTLS device offers hardware-based secure key storage to ensure that a product with the firmware runs. The devices can perform both the Symmetric authentication and Asymmetric authentication where the keys are securely stored in the secure element, thereby reducing the adversary’s ability to extract and modify the keys.

#### 2.1.5 General Data Storage
The ATECC608B-TFLXTLS can be used for this purpose by utilizing those data slots where data can be readily read and written. This eliminates the need to add another EEPROM memory device to just store data.

### 2.2 ATECC608B-TFLXTLS Features:

- Microcontroller-agnostic implementation
- Protection against known tamper, side-channel attacks
- Use your own certificate authority
- Apply a unique, trusted, protected and managed device identity
- Simplify logistics of shipping private keys and reduce manufacturing costs
- Leverage multiple use cases with our pre-configured devices
- Deploy the simplicity of thumbprint certificate authentication
- Turn-key code examples
- Leverage Microchip’s secure provisioning service
- JIL rated “high” secure key storage


## 3. References

Noseda, M.; Zimmerli, L.; Schläpfer, T.; Rüst, A. Performance
Analysis of Secure Elements for IoT. IoT 2022, 3, 1–28. https://doi.org/10.3390/iot3010001

Kietzmann P.; Boeckmann L.; Lanzieri L.; Schmidt T. C.; Wahlisch M. A Performance Study of Crypto-Hardware in the Low-end IoT. In Proc. of EWSN, ACM, 2021. https://eprint.iacr.org/2021/058

Schläpfer, T., & Rüst, A. (2019). Security on IoT devices with secure elements. Embedded World Conference 2019 - Proceedings. https://doi.org/10.21256/zhaw-3350

B. Pearson, C. Zou, Y. Zhang, Z. Ling and X. Fu, "SIC2: Securing Microcontroller Based IoT Devices with Low-cost Crypto Coprocessors," 2020 IEEE 26th International Conference on Parallel and Distributed Systems (ICPADS), Hong Kong, 2020, pp. 372-381. https://doi.org/10.1109/ICPADS51040.2020.00057

Natarajan, D., & Dai, W. (2021). SEAL-Embedded: A Homomorphic Encryption Library for the Internet of Things. IACR Transactions on Cryptographic Hardware and Embedded Systems, 2021(3), 756–779. https://doi.org/10.46586/tches.v2021.i3.756-779

Zimmerli, L., Schläpfer, T., & Rüst, A. (2019). Securing the IoT : introducing an evaluation platform for secure elements. Konferenz Internet of Things - Vom Sensor Bis Zur Cloud, Stuttgart, Deutschland, 20. November 2019. https://doi.org/10.21256/zhaw-22447

---
ATECC608B-TFLXTLSS Documentation: 
https://onlinedocs.microchip.com/oxy/GUID-8F01540E-14A9-479E-9D27-FFFB14DE474E-en-US-2/index.html

I2C information: 
https://onlinedocs.microchip.com/oxy/GUID-8F01540E-14A9-479E-9D27-FFFB14DE474E-en-US-2/GUID-80BD3740-C399-4693-B613-5FD7AED119DA.html
