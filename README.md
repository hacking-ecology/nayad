# Nayad Multiparameter Sonde

![](pic/logo_blue_solid.png)

Nayad is an open source device developed to measure the main environmental variables and focused on the flexibility of deployment, reliability of environmental data (water, air, soil, sound and light).
The user-oriented and highly customizable hardware can be used in a wide range of implementation  within diverse fields such as research, education, and industry.
The new version integrates LoRa and GPS, improving its connectivity power and extending the potential for different use cases.

## Security improvement of version 1.0

The new Nayad version has a embedded crypto-chip [ATECC608B](https://ww1.microchip.com/downloads/aemDocuments/documents/SCBU/ProductDocuments/DataSheets/ATECC608B-CryptoAuthentication-Device-Summary-Data-Sheet-DS40002239B.pdf). This cryptographic coprocessor with secure hardware-based key storage is used as a secure element that will be used for device verification on IoT network. Additionally, it adds a layer against tampering, performing advanced cryptographic operations with low energy consumption.

Featrures:
- Complete asymmetric (public/private) key cryptographic signature solution based upon Elliptic Curve Cryptography and the ECDSA signature protocol.
- EEPROM array which can be used for storage of up to 16 keys, certificates, miscellaneous read/write, read-only or secret data, consumption logging and security configurations. 
- Restrict access to the various sections of memory.
- Configuration can be locked to prevent changes.
- Wide array of defense mechanisms specifically designed to prevent physical attacks.


## Nayad version 1.0 details

<img src="https://gitlab.com/hacking-ecology/nayad/-/raw/master/pic/naydaV1.png" width="330" height="299">

| Item | Specification |
| ------ | ------ |
|ESP32 chip|ESP32-D0WD-V3|
|Integrated SPI flash|4 MB|
|Integrated PSRAM|8 MB|
|Operating voltage|3.0 V ~ 3.6 V|
|Available interfaces| UART, I²C, GPIO|
|microSD|SDHC, max. 32GB|
|GPS|ATGM336H|
|LoRa|Ra-01SH|
|Wi-Fi|full TCP/IP stack and microcontroller capability|
|Secure Element|ATECC608B|


## Cerification
Nayad v1.0 is a [certified Open Source Hardware](https://certification.oshwa.org/es000032.html)

![](pic/oshwa_ES0032.png)

----------------------
Nayad v1.0 is licensed under [CERN-OHL-S V.2](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2). **If you modify Nayad v1.0 design you must license the modifications under the CERN-OHL-S v2**. Please see the [CERN-OHL-S V.2 Guide](https://ohwr.org/project/cernohl/-/wikis/uploads/cf37727497ca2b5295a7ab83a40fcf5a/cern_ohl_s_v2_user_guide.pdf) with details about the conditions.

A copy of this license is provided in the LICENSE file.

This documentation describes Nayad Multiparameters v0.1 and is licensed under the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

You may redistribute and modify this documentation under the terms of the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). 
